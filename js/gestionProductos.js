var productosObtenidos;

function getProductos() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();   //Libreria para enviar peticiones https

  //1:Preparando la peticion, 2:Enviando 3:Esperando respuesta 4: Respondido
  //200: OK

  request.onreadystatechange = function(){
      if (this.readyState == 4 && this.status ==200) {
        //Nos responde como una cadena de texto y hay que convertirla a JSON

        //console.table(JSON.parse(request.responseText).value);

        //Obtiene la respuesta de la peticion en "Cadena de caracteres"
        productosObtenidos = request.responseText;
        procesarProductos();
      }
  }

  request.open("GET",url, true);
  request.send();
}

function procesarProductos() {

  //Conversion de la cadena de caraceres a formato JSON
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  for (var i = 0; i < JSONProductos.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

  tabla.classList.add("table");
  tabla.classList.add("table-striped");
}
